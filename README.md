# squiddy

A small tool to inspect a web servers. 
Supports Getting vendor and version as well as checking if directory retrieval is enabled 

![alt text](https://static.wikia.nocookie.net/robotsupremacy/images/0/08/Squiddy.jpg/revision/latest/scale-to-width-down/500?cb=20120512111003)

## install

[Step 1: Setup a Python Env - Click for an example](https://realpython.com/effective-python-environment/)


Step 2: Install dependencies with the following command
```
pip install -r  requirements.txt
```

Step 3: Use the tool!
```
python squiddy.py scan 1.2.3.4
```

```
python squiddy.py scan --input test_input.txt
```

```
python squiddy.py scan --help 
```

```
python squiddy.py scan 1.2.3.4 --flashpoint
```


For Development -

Install Dependencies
```
pip install -r  dev-requirements.txt
```

Run Tests
```
coverage run --source=. -m pytest
```

Check Coverage
```
coverage report -m
```