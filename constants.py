import re


class SnakeCase:
    DIRECTORY_LISTING = 'directory_listing'


class MacroCase:
    HTTP = 'HTTP'
    HTTPS = 'HTTPS'


class LowerCase:
    HOST = 'host'
    PROTOCOL = 'protocol'
    APACHE = 'apache'
    NGINX = 'nginx'
    GWS = 'gws'
    IIS = 'iis'
    TITLE = 'title'
    H1 = 'h1'
    SERVER = 'server'
    VERSION = 'version'
    UNKNOWN = 'unknown'
    ERROR = 'error'


class Regex:
    semantic_version = re.compile(
        '(0|[1-9]\d*)\.(0|[1-9]\d*)\.(0|[1-9]\d*)(?:-((?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(?:\.(?:0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?')
