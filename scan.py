import logging
import re

import requests
import urllib3
from bs4 import BeautifulSoup
from requests.exceptions import SSLError, ConnectTimeout, ConnectionError, ReadTimeout
from typing import Match


from constants import Regex, LowerCase, SnakeCase, MacroCase

urllib3.disable_warnings()

logger = logging.getLogger(__name__)


def scan_host(host: str, protocol: str, timeout=None) -> dict:
    """
    Core function to execute scan

    :param protocol:
    :param timeout:
    :param host:
    :return:
    """

    result = {LowerCase.HOST: host,
              LowerCase.PROTOCOL: protocol}
    if protocol is MacroCase.HTTP:
        response = get_response(f'http://{host}')
    else:
        response = get_response(f'https://{host}')

    if LowerCase.ERROR in response.keys():
        result[LowerCase.ERROR] = response[LowerCase.ERROR]
        return result
    else:
        return {**result, **response}


def check_directory_listing(content: str) -> bool:
    """
    From what I can tell there really is not an easy way to ID if directory browsing is enabled.
    This feels like kludge, but it works for now.

    :param content:
    :return:
    """
    soup = BeautifulSoup(content, features="html.parser")
    identifier = 'Index of /'
    if identifier in str(soup.find_all(LowerCase.H1)):
        return True
    elif identifier in str(soup.find_all(LowerCase.TITLE)):
        return True
    else:
        return False


def parse_response(response: requests.Response) -> dict:
    """
    Parse Requests Response
    :param response:
    :return:
    """
    result = {}

    if response.status_code == 403:
        result[SnakeCase.DIRECTORY_LISTING] = False
    elif response.status_code == 200:
        result[SnakeCase.DIRECTORY_LISTING] = check_directory_listing(response.text)

    server_header = response.headers['Server']

    if extract_server(server_header):
        result[LowerCase.SERVER] = extract_server(server_header)
    else:
        result[LowerCase.SERVER] = server_header

    result[LowerCase.VERSION] = extract_version(server_header)

    return result


def get_response(host, timeout=3):
    """
    preform get request against server
    :param host:
    :return:
    """

    try:
        response = requests.get(host, timeout=timeout, verify=False)
        return parse_response(response)
    except (ConnectionError, ConnectTimeout, SSLError, ReadTimeout) as error:
        if type(error).__name__ == 'ConnectionError':
            logger.debug(f"ConnectionError with to: {host}")
        elif type(error).__name__ == 'ConnectTimeout':
            logger.debug(f"Timeout Connecting to: {host}")
        elif type(error).__name__ == 'SSLError':
            logger.debug(f"SSL Error connecting to: {host}")
        elif type(error).__name__ == 'ReadTimeout':
            logger.debug(f"SSL Error connecting to: {host}")
        return {LowerCase.ERROR: type(error).__name__}


def extract_version(server_header: str) -> Match or None:
    """
    extract server version from response header

    :param server_header:
    :return:
    """
    if re.search(Regex.semantic_version, server_header):
        return re.search(Regex.semantic_version, server_header).group(0)


def extract_server(server_header: str):
    """
    extract server type from response header

    :param server_header:
    :return:
    """
    if LowerCase.APACHE in server_header.lower():
        return LowerCase.APACHE
    elif LowerCase.NGINX in server_header.lower():
        return LowerCase.NGINX
    elif LowerCase.IIS in server_header.lower():
        return LowerCase.IIS
    elif LowerCase.GWS in server_header.lower():
        return LowerCase.GWS
    return LowerCase.UNKNOWN
