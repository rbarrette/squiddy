#!/usr/bin/env python
import logging
import click

from constants import MacroCase
from scan import scan_host
from utility import flashpoint_filter, import_file
from tabulate import tabulate
from timeit import default_timer as timer

logger = logging.getLogger(__name__)


@click.group()
@click.option('--debug', default=False, is_flag=True, help='Enable Debugging')
def cli(debug) -> None:
    if debug:
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.INFO)


@cli.command()
@click.argument('hosts', nargs=-1, default=None)
@click.option('--input', default=None, type=click.Path(exists=True), help='path to an input file')
@click.option('--flashpoint', default=False, is_flag=True, help='filter servers with directory listing enabled or running IIS version 7.0.x or Nginx 1.2.x')
def scan(hosts: list, input: str, flashpoint: bool) -> None:
    """
    core scan command

    :return: None
    """
    if hosts and input:
        raise click.UsageError('Provide either addresses or an input file, not both!', ctx=None)
    if hosts:
        logger.debug(f'scan called with the following hosts:{hosts}')
        results = scan_hosts(hosts)

    if input:
        logger.debug(f'scan called with the following input file:{input}')
        hosts = import_file(input)
        results = scan_hosts(hosts)

    if flashpoint:
        filtered = flashpoint_filter(results)
        if filtered:
            click.echo(tabulate(filtered, tablefmt='simple', headers="keys"))
        else:
            click.echo("no results matching flashpoint criteria")
    else:
        click.echo(tabulate(results, tablefmt='simple', headers="keys"))


def scan_hosts(hosts: list) -> list[dict]:
    """
    Scan all hosts passed in. HTTP/HTTPS scans preformed for each
    :param hosts:
    :return:
    """
    results = []
    with click.progressbar(hosts, label='Scanning Web Servers', length=len(hosts)) as bar:
        time_start = timer()
        for host in bar:
            results.append(scan_host(host, protocol=MacroCase.HTTP))
            results.append(scan_host(host, protocol=MacroCase.HTTPS))
        time_end = timer()
    click.echo(f'scan completed in {time_end - time_start} seconds')
    return results


cli.add_command(scan)

if __name__ == "__main__":
    cli()
