"""Module containing unit tests for utility"""
import unittest
from unittest.mock import patch, MagicMock, call

from constants import LowerCase
from scan import scan_host, check_directory_listing, parse_response, get_response, extract_version, \
    extract_server
from requests.exceptions import SSLError, ConnectTimeout, ConnectionError, ReadTimeout


class TestScan(unittest.TestCase):
    """Class containing scan functions to test"""

    @patch('scan.get_response')
    def test_scan_host_http(self, mock_get_response):
        """test http proto scan_host"""
        expected_result = {'host': '1.2.3.4', 'protocol': 'HTTP', 'directory_listing': True, 'server': 'nginx',
                           'version': '1.18.0'}
        mock_get_response.return_value = {'directory_listing': True, 'server': 'nginx', 'version': '1.18.0'}
        result = scan_host(host='1.2.3.4', protocol='HTTP')
        self.assertEqual(result, expected_result)

    @patch('scan.get_response')
    def test_scan_host_https(self, mock_get_response):
        """test https proto scan_host"""
        expected_result = {'host': '1.2.3.4', 'protocol': 'HTTPS', 'directory_listing': True, 'server': 'nginx',
                           'version': '1.18.0'}
        mock_get_response.return_value = {'directory_listing': True, 'server': 'nginx', 'version': '1.18.0'}
        result = scan_host(host='1.2.3.4', protocol='HTTPS')
        self.assertEqual(result, expected_result)

    @patch('scan.get_response')
    def test_scan_host_error(self, mock_get_response):
        """test error scan_host"""
        expected_result = {'host': '1.2.3.4', 'protocol': 'HTTPS', 'error': 'SSLError'}
        mock_get_response.return_value = {'error': 'SSLError'}
        result = scan_host(host='1.2.3.4', protocol='HTTPS')
        self.assertEqual(result, expected_result)

    def test_check_directory_listing_h1_match(self):
        content = '<html>\r\n<head><title> of /slackware/</title></head>\r\n<body>\r\n<h1>Index of ' \
                  '/slackware/</h1><hr><pre><a href="../">../</a>\r\n</pre><hr></body>\r\n</html>\r\n '
        self.assertTrue(check_directory_listing(content))

    def test_check_directory_listing_title_match(self):
        content = '<html>\r\n<head><title>Index of /slackware/</title></head>\r\n<body>\r\n<h1> ' \
                  '/slackware/</h1><hr><pre><a href="../">../</a>\r\n</pre><hr></body>\r\n</html>\r\n '
        self.assertTrue(check_directory_listing(content))

    def test_check_directory_listing_no_match(self):
        content = '<html>\r\n<head><title> /slackware/</title></head>\r\n<body>\r\n<h1> ' \
                  '/slackware/</h1><hr><pre><a href="../">../</a>\r\n</pre><hr></body>\r\n</html>\r\n '
        self.assertFalse(check_directory_listing(content))

    @patch('scan.extract_server')
    @patch('scan.extract_version')
    @patch('scan.check_directory_listing')
    def test_parse_response_200(self, mock_directory, mock_version, mock_server):
        mock_response = MagicMock()
        mock_response.headers = {'Server': 'nginx/1.18.0 (Ubuntu)', 'Date': 'Fri, 24 Sep 2021 15:46:33 GMT',
                                 'Content-Type': 'text/html', 'Transfer-Encoding': 'chunked',
                                 'Connection': 'keep-alive', 'Content-Encoding': 'gzip'}
        mock_response.status_code = 200
        mock_server.return_value = 'nginx'
        mock_version.return_value = '1.18.0'
        mock_directory.return_value = True
        expected_result = {'directory_listing': True, 'server': 'nginx', 'version': '1.18.0'}
        result = parse_response(mock_response)

        mock_version.assert_has_calls([call('nginx/1.18.0 (Ubuntu)')])
        mock_server.assert_has_calls([call('nginx/1.18.0 (Ubuntu)'), call('nginx/1.18.0 (Ubuntu)')])
        self.assertEqual(result, expected_result)

    @patch('scan.extract_server')
    @patch('scan.extract_version')
    def test_parse_response_403(self, mock_version, mock_server):
        mock_response = MagicMock()
        mock_response.headers = {'Server': 'nginx/1.18.0 (Ubuntu)', 'Date': 'Fri, 24 Sep 2021 15:46:33 GMT',
                                 'Content-Type': 'text/html', 'Transfer-Encoding': 'chunked',
                                 'Connection': 'keep-alive', 'Content-Encoding': 'gzip'}
        mock_response.status_code = 403
        mock_server.return_value = None
        mock_version.return_value = '1.18.0'
        expected_result = {'directory_listing': False, 'server': 'nginx/1.18.0 (Ubuntu)', 'version': '1.18.0'}
        result = parse_response(mock_response)

        mock_version.assert_has_calls([call('nginx/1.18.0 (Ubuntu)')])
        mock_server.assert_has_calls([call('nginx/1.18.0 (Ubuntu)')])
        self.assertEqual(result, expected_result)

    @patch('scan.requests')
    @patch('scan.parse_response')
    def test_get_response_happy_path(self, mock_parse, mock_requests):
        mock_requests.get.return_value = 'your a winner'
        mock_parse.return_value = 'something back'
        result = get_response('something.com')
        expected = 'something back'
        self.assertEqual(result, expected)
        mock_parse.assert_has_calls([call('your a winner')])
        mock_requests.assert_has_calls([call.get('something.com', timeout=3, verify=False)])

    @patch('scan.requests')
    def test_get_response_connection_error(self, mock_requests):
        mock_requests.get.side_effect = ConnectionError
        result = get_response('something.com')
        expected = {'error': 'ConnectionError'}
        self.assertEqual(result, expected)


    @patch('scan.requests')
    def test_get_response_connection_timeout(self, mock_requests):
        mock_requests.get.side_effect = ConnectTimeout
        result = get_response('something.com')
        expected = {'error': 'ConnectTimeout'}
        self.assertEqual(result, expected)

    @patch('scan.requests')
    def test_get_response_ssl_error(self, mock_requests):
        mock_requests.get.side_effect = SSLError
        result = get_response('something.com')
        expected = {'error': 'SSLError'}
        self.assertEqual(result, expected)

    @patch('scan.requests')
    def test_get_response_read_timeout(self, mock_requests):
        mock_requests.get.side_effect = ReadTimeout
        result = get_response('something.com')
        expected = {'error': 'ReadTimeout'}
        self.assertEqual(result, expected)

    def test_extract_version(self):
        version = ' asd 1.2.1 adsd'
        self.assertEqual('1.2.1', extract_version(version))

    def test_extract_version_no_match(self):
        version = 'not a real sem ver'
        self.assertIsNone(extract_version(version))

    def test_extract_server_apache(self):
        server_header = 'Apache 1.2.3 something'
        self.assertEqual(LowerCase.APACHE, extract_server(server_header))

    def test_extract_server_nginx(self):
        server_header = 'NGINX 1.2.3 something'
        self.assertEqual(LowerCase.NGINX, extract_server(server_header))

    def test_extract_server_iis(self):
        server_header = 'Microsoft IIS 1.2.3 something'
        self.assertEqual(LowerCase.IIS, extract_server(server_header))

    def test_extract_server_gws(self):
        server_header = 'gws dasmmlkjda'
        self.assertEqual(LowerCase.GWS, extract_server(server_header))

    def test_extract_server_unknown(self):
        server_header = 'dasmmlkjda'
        self.assertEqual(LowerCase.UNKNOWN, extract_server(server_header))