import unittest
from unittest.mock import patch, call

from click.testing import CliRunner
from squiddy import cli


class TestScan(unittest.TestCase):

    @patch('squiddy.scan_host')
    def test_scan(self, mock_scan_host):
        mock_scan_host.side_effect = [
            {'host': 'mirror.csclub.uwaterloo.ca/slackware/', 'protocol': 'HTTP', 'directory_listing': True,
             'server': 'nginx', 'version': '1.18.0'},
            {'host': 'mirror.csclub.uwaterloo.ca/slackware/', 'protocol': 'HTTPS', 'directory_listing': True,
             'server': 'nginx', 'version': '1.18.0'}
        ]
        runner = CliRunner()
        result = runner.invoke(cli, ['scan', '1.2.3.4'])
        mock_scan_host.assert_has_calls([call('1.2.3.4', protocol='HTTP'), call('1.2.3.4', protocol='HTTPS')])
        assert result.exit_code == 0

    @patch('squiddy.scan_host')
    def test_scan_debug(self, mock_scan_host):
        mock_scan_host.side_effect = [
            {'host': 'mirror.csclub.uwaterloo.ca/slackware/', 'protocol': 'HTTP', 'directory_listing': True,
             'server': 'nginx', 'version': '1.18.0'},
            {'host': 'mirror.csclub.uwaterloo.ca/slackware/', 'protocol': 'HTTPS', 'directory_listing': True,
             'server': 'nginx', 'version': '1.18.0'}
        ]
        runner = CliRunner()
        result = runner.invoke(cli, ['--debug', 'scan', '1.2.3.4'])
        mock_scan_host.assert_has_calls([call('1.2.3.4', protocol='HTTP'), call('1.2.3.4', protocol='HTTPS')])
        assert result.exit_code == 0

    @patch('squiddy.scan_host')
    @patch('squiddy.flashpoint_filter')
    def test_scan_flashpoint_none(self, mock_flashpoint, mock_scan_host):
        mock_scan_host.side_effect = [
            {'host': 'mirror.csclub.uwaterloo.ca/slackware/', 'protocol': 'HTTP', 'directory_listing': True,
             'server': 'nginx', 'version': '1.18.0'},
            {'host': 'mirror.csclub.uwaterloo.ca/slackware/', 'protocol': 'HTTPS', 'directory_listing': True,
             'server': 'nginx', 'version': '1.18.0'}
        ]
        mock_flashpoint.return_value = None
        runner = CliRunner()
        result = runner.invoke(cli, ['--debug', 'scan', '1.2.3.4', '--flashpoint'])
        mock_scan_host.assert_has_calls([call('1.2.3.4', protocol='HTTP'), call('1.2.3.4', protocol='HTTPS')])
        assert 'no results' in result.output
        assert result.exit_code == 0

    @patch('squiddy.scan_hosts')
    @patch('squiddy.flashpoint_filter')
    def test_scan_flashpoint_happy(self, mock_flashpoint, mock_scan_hosts):
        mock_scan_hosts.side_effect = [
            {'host': 'mirror.csclub.uwaterloo.ca/slackware/', 'protocol': 'HTTP', 'directory_listing': True,
             'server': 'nginx', 'version': '1.18.0'},
            {'host': 'mirror.csclub.uwaterloo.ca/slackware/', 'protocol': 'HTTPS', 'directory_listing': True,
             'server': 'nginx', 'version': '1.18.0'}
        ]
        mock_flashpoint.return_value = [
            {'host': 'mirror.csclub.uwaterloo.ca/slackware/', 'protocol': 'HTTP', 'directory_listing': True,
             'server': 'nginx', 'version': '1.18.0'},
            {'host': 'mirror.csclub.uwaterloo.ca/slackware/', 'protocol': 'HTTPS', 'directory_listing': True,
             'server': 'nginx', 'version': '1.18.0'}
        ]
        runner = CliRunner()
        result = runner.invoke(cli, ['--debug', 'scan', '1.2.3.4', '5.6.7.8', '--flashpoint'])
        mock_scan_hosts.assert_has_calls([call(('1.2.3.4', '5.6.7.8'))])
        assert result.exit_code == 0

    def test_scan_flashpoint_error_host_and_input(self):
        runner = CliRunner()
        with runner.isolated_filesystem():
            with open('file.txt', 'w') as f:
                f.write('1.2.3.4')
            result = runner.invoke(cli, ['--debug', 'scan', '1.2.3.4', '--input', 'file.txt'])
            assert result.exit_code == 2

    @patch('squiddy.scan_hosts')
    @patch('squiddy.import_file')
    def test_scan_input(self, mock_import_file, mock_scan_hosts):
        mock_import_file.return_value = ['1.2.3.4', '5.6.4.8']
        runner = CliRunner()
        with runner.isolated_filesystem():
            with open('file.txt', 'w') as f:
                f.write('1.2.3.4')
            result = runner.invoke(cli, ['--debug', 'scan', '--input', 'file.txt'])
            mock_scan_hosts.assert_called_with(['1.2.3.4', '5.6.4.8'])
            assert result.exit_code == 0
