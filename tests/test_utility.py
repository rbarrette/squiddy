"""Module containing unit tests for utility"""
import unittest
from unittest.mock import mock_open, patch

from utility import flashpoint_filter, import_file


class TestFlashpointFilter(unittest.TestCase):
    """Class for testing Flashpoint filter"""

    def test_flashpoint_filter(self):
        """test flashpoint_filter"""
        test_results = [{'host': 'mirror.csclub.uwaterloo.ca/slackware/', 'protocol': 'HTTP', 'directory_listing': True,
                         'server': 'nginx', 'version': '1.18.0'},
                        {'host': 'mirror.csclub.uwaterloo.ca/slackware/', 'protocol': 'HTTPS',
                         'directory_listing': False, 'server': 'nginx', 'version': '1.2.13'},
                        {'host': 'cdimage.debian.org/debian-cd/', 'protocol': 'HTTP', 'directory_listing': True,
                         'server': 'apache', 'version': '2.4.46'},
                        {'host': 'cdimage.debian.org/debian-cd/', 'protocol': 'HTTPS', 'directory_listing': True,
                         'server': 'apache', 'version': '2.4.46'},
                        {'host': 'dl.fedoraproject.org/pub/epel/', 'protocol': 'HTTP', 'directory_listing': True,
                         'server': 'apache', 'version': None},
                        {'host': 'dl.fedoraproject.org/pub/epel/', 'protocol': 'HTTPS', 'directory_listing': True,
                         'server': 'apache', 'version': None},
                        {'host': 'google.com', 'protocol': 'HTTP', 'directory_listing': False, 'server': 'gws',
                         'version': None},
                        {'host': 'google.com', 'protocol': 'HTTPS', 'directory_listing': False, 'server': 'gws',
                         'version': None},
                        {'host': '127.0.0.1:55000/', 'protocol': 'HTTP', 'directory_listing': False, 'server': 'nginx',
                         'version': '1.21.3'}, {'host': '127.0.0.1:55000/', 'protocol': 'HTTPS', 'error': 'SSLError'},
                        {'host': '127.0.0.1:8080', 'protocol': 'HTTP', 'directory_listing': False, 'server': 'iis',
                         'version': '7.0.1'}, {'host': '127.0.0.1:8080', 'protocol': 'HTTPS', 'error': 'SSLError'}]

        expected_results = [{'directory_listing': True,
                             'host': 'mirror.csclub.uwaterloo.ca/slackware/',
                             'protocol': 'HTTP',
                             'server': 'nginx',
                             'version': '1.18.0'},
                            {'directory_listing': False,
                             'host': 'mirror.csclub.uwaterloo.ca/slackware/',
                             'protocol': 'HTTPS',
                             'server': 'nginx',
                             'version': '1.2.13'},
                            {'directory_listing': True,
                             'host': 'cdimage.debian.org/debian-cd/',
                             'protocol': 'HTTP',
                             'server': 'apache',
                             'version': '2.4.46'},
                            {'directory_listing': True,
                             'host': 'cdimage.debian.org/debian-cd/',
                             'protocol': 'HTTPS',
                             'server': 'apache',
                             'version': '2.4.46'},
                            {'directory_listing': True,
                             'host': 'dl.fedoraproject.org/pub/epel/',
                             'protocol': 'HTTP',
                             'server': 'apache',
                             'version': None},
                            {'directory_listing': True,
                             'host': 'dl.fedoraproject.org/pub/epel/',
                             'protocol': 'HTTPS',
                             'server': 'apache',
                             'version': None},
                            {'directory_listing': False,
                             'host': '127.0.0.1:8080',
                             'protocol': 'HTTP',
                             'server': 'iis',
                             'version': '7.0.1'}]

        self.assertEqual(expected_results, flashpoint_filter(test_results))


class TestImportFile(unittest.TestCase):
    """Class for testing Flashpoint filter"""

    @patch("builtins.open", new_callable=mock_open, read_data='1.2.3.4')
    def test_import_file(self, mock_file):
        """test flashpoint_filter"""
        assert import_file("path/to/open") == ['1.2.3.4']
        mock_file.assert_called_with("path/to/open",  'r')