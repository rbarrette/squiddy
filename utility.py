import logging

from constants import LowerCase, SnakeCase
from packaging.version import Version
from packaging.version import parse as parse_version

logger = logging.getLogger(__name__)


def flashpoint_filter(results: list) -> list:
    """
    Flashpoint Requested filtering

    which addresses are hosting IIS version 7.0.x or Nginx 1.2.x.
    as well as host that have enabled directory listing

    :param results:
    :return:
    """
    output = []
    for result in results:
        if LowerCase.ERROR in result.keys():
            continue

        if result[SnakeCase.DIRECTORY_LISTING] is True:
            output.append(result)
            continue

        if result[LowerCase.VERSION] is None:
            continue

        version = parse_version(result[LowerCase.VERSION])
        if result[LowerCase.SERVER] is LowerCase.IIS:
            if (version >= Version('7.0.0')) and (version < Version('7.1.0')):
                output.append(result)
        if result[LowerCase.SERVER] is LowerCase.NGINX:
            if (version >= Version('1.2.0')) and (version < Version('1.3.0')):
                output.append(result)

    return output


def import_file(file):
    """quick function to read in lines from a file"""
    _ = open(file, 'r')
    return _.readlines()
